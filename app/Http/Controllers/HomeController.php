<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use thiagoalessio\TesseractOCR\TesseractOCR;


class HomeController extends Controller
{
    public function index(Request $request)
    {
        $path=$request->file('image')->store(str_random(8));
//        dd(storage_path('app/').$path);
        echo (new TesseractOCR(storage_path('app/').$path))
            ->run();
    }
}
